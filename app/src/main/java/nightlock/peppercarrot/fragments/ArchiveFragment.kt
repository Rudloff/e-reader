/*
 * Copyright (C) 2017 - 2018 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Reader for Pepper&Carrot.
 *
 * Reader for Pepper&Carrot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nightlock.peppercarrot.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.github.mthli.sugartask.SugarTask
import kotlinx.android.synthetic.main.fragment_archive.*
import nightlock.peppercarrot.R
import nightlock.peppercarrot.adapters.ArchiveAdapter
import nightlock.peppercarrot.utils.ArchiveDataManager
import nightlock.peppercarrot.utils.Language
import java.io.IOException

/**
 * Fragment holding a list of Episode covers of a given Language
 * Created by Jihoon Kim on 4/30/17.
 */

class ArchiveFragment : Fragment(){
    private lateinit var archiveAdapter: ArchiveAdapter
    private lateinit var db: ArchiveDataManager

    override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)

        if (arguments != null) {
            archiveAdapter = ArchiveAdapter(arguments!!.getParcelable(ARG_LANGUAGE))
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              bundle: Bundle?
    ): View =
        inflater.inflate(R.layout.fragment_archive, container, false)

    override fun onViewCreated(view: View, bundle: Bundle?) {
        super.onViewCreated(view, bundle)
        db = ArchiveDataManager(context!!)
        init()
        if (bundle != null && bundle.containsKey("pos"))
            archive_recycler.layoutManager.onRestoreInstanceState(bundle.getParcelable("pos"))
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable("pos", archive_recycler.layoutManager.onSaveInstanceState())
    }

    private fun init() {
        archive_recycler.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = archiveAdapter
        }

        if (db.length() < 1)
            initArchive()
        else
            loaded()
    }

    private fun loaded() = db.getAll().forEach { archiveAdapter.addAndNotify(it) }

    private fun initArchive() {
        archive_progress.showLoading()

        SugarTask
                .with(this)
                .assign {
                    ArchiveDataManager.updateArchive(context!!)
                }.finish { _ ->
                    archive_progress.showContent()
                    loaded()
                }.broken { e ->
                    if (e is IOException)
                        onError(e)
                    else throw e
                }.execute()
    }

    private fun onError(e: Exception) {
        archive_progress.showError(R.drawable.ic_signal_wifi_off_black_24dp, "Network Error",
                "Connect to network and try again", "Retry", { _ ->
            initArchive()
        })
        Log.e("crystal_ball", "Error on initArchive()")
        e.printStackTrace()
    }

    companion object {
        private const val ARG_LANGUAGE = "language"

        fun newInstance(language: Language): ArchiveFragment{
            val fragment = ArchiveFragment()
            val args = Bundle()
            args.putParcelable(ARG_LANGUAGE, language)
            fragment.arguments = args
            return fragment
        }
    }
}

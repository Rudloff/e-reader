/*
 * Copyright (C) 2017 - 2018 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Reader for Pepper&Carrot.
 *
 * Reader for Pepper&Carrot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package nightlock.peppercarrot.fragments

import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.davemorrissey.labs.subscaleview.ImageSource
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView
import com.squareup.picasso.Picasso
import io.github.mthli.sugartask.SugarTask
import kotlinx.android.synthetic.main.fragment_comic_viewer.*
import nightlock.peppercarrot.R
import nightlock.peppercarrot.activities.ComicViewerActivity
import nightlock.peppercarrot.utils.Decoder
import nightlock.peppercarrot.utils.RegionDecoder
import nightlock.peppercarrot.utils.isConnected
import nightlock.peppercarrot.utils.thisOrThat
import okhttp3.OkHttpClient

/**
 * Created by nightlock on 5/7/17.
 */

class ComicViewerFragment: Fragment() {
    private val anim by lazy { AnimationUtils.loadAnimation(context, R.anim.blink) }
    private lateinit var imgLink: String

    override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)

        if (arguments != null) {
            imgLink = arguments!!.getString(ComicViewerFragment.ARG_LINK)
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.fragment_comic_viewer, container, false)
        SugarTask
                .with(this@ComicViewerFragment)
                .assign {
                    if (isConnected(context!!)) {
                        imgLink = thisOrThat("$imgLink.jpg", "$imgLink.gif")!!
                    }
                }.finish {
                    comic_image.setImage(ImageSource.uri(imgLink))
                }.execute()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        comic_image.apply {
            setBitmapDecoderFactory { Decoder(Picasso.with(context)) }
            setRegionDecoderFactory { RegionDecoder(OkHttpClient()) }

            setOnClickListener { _ ->
                val activity = activity as ComicViewerActivity
                activity.toggle()
            }
            setOnImageEventListener(object : SubsamplingScaleImageView.DefaultOnImageEventListener() {
                override fun onReady() {
                    super.onReady()
                    setBackgroundColor(Color.WHITE)
                }
            })
        }

        loading_image.startAnimation(anim)
    }

    companion object {
        private const val ARG_LINK = "link"

        fun newInstance(link: String): ComicViewerFragment {
            val fragment = ComicViewerFragment()
            val args = Bundle().apply {
                putString(ARG_LINK, link)
            }
            fragment.arguments = args
            return fragment
        }
    }
}

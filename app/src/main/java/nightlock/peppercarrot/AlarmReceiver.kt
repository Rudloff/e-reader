/*
 * Copyright (C) 2017 - 2018 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Reader for Pepper&Carrot.
 *
 * Reader for Pepper&Carrot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nightlock.peppercarrot

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.support.v7.preference.PreferenceManager
import nightlock.peppercarrot.activities.MainActivity
import nightlock.peppercarrot.utils.ArchiveDataManager
import nightlock.peppercarrot.utils.LanguageDataManager
import nightlock.peppercarrot.utils.isConnected
import nightlock.peppercarrot.utils.setAlarm

/**
 * Broadcast Receiver for updating comic list.
 * Created by graphene on 24/08/17.
 */

class AlarmReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        if (context == null) return //If context is not available the whole code will not work.

        if (intent?.action == "android.intent.action.BOOT_COMPLETED") {
            setAlarm(context)
            return
        }

        if (!isConnected(context)) return

        val preference = PreferenceManager.getDefaultSharedPreferences(context)
        if (!preference.getBoolean("download_on_mobile_network", false)) {
            val networkInfo = context.getSystemService(Context.CONNECTIVITY_SERVICE)
                    as ConnectivityManager
            if (networkInfo.activeNetworkInfo.type == ConnectivityManager.TYPE_MOBILE)
                return
        }

        val notification = NotificationCompat
                .Builder(context, MainActivity.UPDATE_NOTIF_ID)
                .setSmallIcon(R.drawable.ic_launcher_web)
                .setContentTitle("Boop")
                .setContentText("Updates Ahoy!")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .build()

        Thread {
            context.let {
                ArchiveDataManager.updateArchive(it)
                LanguageDataManager.updateLanguage(it)
            }
        }.start()

        NotificationManagerCompat
                .from(context)
                .notify(NOTIF_ID, notification)

    }

    companion object {
        const val NOTIF_ID = 42
    }
}

/*
 * Copyright (C) 2017 - 2018 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Reader for Pepper&Carrot.
 *
 * Reader for Pepper&Carrot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nightlock.peppercarrot.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import nightlock.peppercarrot.utils.LanguageDataManager
import nightlock.peppercarrot.utils.getPreferredLanguage
import nightlock.peppercarrot.viewholders.LanguageSelectionViewHolder

/**
 *
 */
class LanguageSelectionRecyclerViewAdapter(context: Context) :
        RecyclerView.Adapter<LanguageSelectionViewHolder>() {
    private val db = LanguageDataManager(context)
    private val languageList = db.getAll()
    private val preferredLanguage = getPreferredLanguage(languageList)!!

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LanguageSelectionViewHolder =
            LanguageSelectionViewHolder.newInstance(parent)


    override fun onBindViewHolder(holder: LanguageSelectionViewHolder, position: Int) {
        val language = languageList[position]
        val isRecommended = language == preferredLanguage
        holder.onBind(Pair(language, isRecommended))
    }

    override fun getItemCount(): Int = languageList.size
}

/*
 * Copyright (C) 2017 - 2018 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Reader for Pepper&Carrot.
 *
 * Reader for Pepper&Carrot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nightlock.peppercarrot.utils

import android.content.ContentValues
import android.content.Context
import android.database.Cursor

/**
 * SQLite Database Helper Class for Storing Episode Information.
 * Created by Jihoon Kim on 05/06/17.
 */
class ArchiveDataManager(context: Context) : BaseCacheDataManager<Episode>(context, DATABASE_NAME,
        TABLE_NAME, DATABASE_VERSION, COLUMN_NAME_INDEX, COLUMN_MAP) {

    companion object {
        const val DATABASE_NAME = "episodeDB.db"
        const val DATABASE_VERSION = 5
        const val TABLE_NAME = "episodeEntry"
        const val COLUMN_NAME_INDEX = "_id"
        const val COLUMN_NAME_NAME = "name"
        const val COLUMN_NAME_PAGES = "pages"
        const val COLUMN_NAME_PNC_LANGUAGES = "pnc_languages"
        val COLUMN_MAP = mapOf(
                COLUMN_NAME_INDEX to "INTEGER PRIMARY KEY",
                COLUMN_NAME_NAME to "TEXT",
                COLUMN_NAME_PAGES to "INTEGER",
                COLUMN_NAME_PNC_LANGUAGES to "TEXT"
        )

        fun updateArchive(context: Context) {
            val db = ArchiveDataManager(context)
            for (episode in getEpisodeList()) db.put(episode)
            db.close()
        }
    }

    fun get(index: Int): Episode = get(index.toString())

    override fun getFromCursor(cursor: Cursor): Episode {
        val index = cursor.getInt(0)
        val name = cursor.getString(1)
        val pages = cursor.getInt(2)
        val pncLangs = cursor.getString(3).split(",")

        return Episode(index, name, pages, pncLangs)
    }

    override fun toContentValues(data: Episode): ContentValues = ContentValues().apply {
        put(COLUMN_NAME_INDEX, data.index)
        put(COLUMN_NAME_NAME, data.name)
        put(COLUMN_NAME_PAGES, data.pages)
        put(COLUMN_NAME_PNC_LANGUAGES, data.supported_languages.joinToString(","))
    }

    override fun getIdValue(data: Episode): String = data.index.toString()

    /*fun getTotalPageCount(): Int {
        var pageCount = 0
        getAll().forEach { episode -> pageCount += episode.pages }
        return pageCount
    }*/
}

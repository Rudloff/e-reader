/*
 * Copyright (c) 2018 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Reader for Pepper&Carrot.
 *
 * Reader for Pepper&Carrot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package nightlock.peppercarrot.utils

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

@Suppress("IMPLICIT_CAST_TO_ANY")
abstract class BaseCacheDataManager<T>(context: Context, dbName: String,
        private val tableName: String, version: Int, private val idColumn: String,
        columns: Map<String, String>) : SQLiteOpenHelper(context, dbName, null, version) {

    private val columnNames = columns.keys.toTypedArray()
    private val sqlCreateEntries = "CREATE TABLE $tableName(" +
            "${columns.forEach { "${it.key} ${it.value}, " }})"
    private val sqlDeleteEntries = "DROP TABLE IF EXISTS $tableName"
    private val sqlSelectAll = "SELECT * FROM $tableName"

    override fun onCreate(db: SQLiteDatabase) = db.execSQL(sqlCreateEntries)

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(sqlDeleteEntries)
        onCreate(db)
    }

    fun put(data: T) {
        writableDatabase.use { db ->
            //Check for duplicates and updates.
            var entryExists = false
            val cursor = db.query(tableName, columnNames, "$idColumn = ?",
                    arrayOf(getIdValue(data)), null, null, null, null)
            cursor.use{
                entryExists = cursor.moveToFirst()
                if (entryExists && getFromCursor(cursor) == data) return //Identical ROW exists; return
            }

            val values = toContentValues(data)

            if (entryExists) db.update(tableName, values,
                    "$idColumn = ?", arrayOf(getIdValue(data)))
            else db.insert(tableName, null, values)
        }
    }

    fun length(): Int {
        readableDatabase.use { db ->
            db.rawQuery(sqlSelectAll, null).use { cursor ->
                return cursor.count
            }
        }
    }

    fun get(id: String): T = get(idColumn, id)

    fun get(name: String, value: String): T {
        readableDatabase.use { db ->
            db.query(tableName, columnNames, "$name = ?", arrayOf(value),
                    null, null, null, null).use { cursor ->
                cursor.moveToFirst()
                return getFromCursor(cursor)
            }
        }
    }

    fun getAll(): List<T> {
        val list = ArrayList<T>()
        val db = readableDatabase
        val cursor = db.rawQuery(sqlSelectAll, null)

        if (cursor.moveToFirst())
            do list += getFromCursor(cursor) while (cursor.moveToNext())

        return list
    }

    abstract fun getFromCursor(cursor: Cursor): T
    abstract fun toContentValues(data: T): ContentValues
    abstract fun getIdValue(data: T): String
}

/*
 * Copyright (C) 2017 - 2018 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Reader for Pepper&Carrot.
 *
 * Reader for Pepper&Carrot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nightlock.peppercarrot.utils

import android.content.ContentValues
import android.content.Context
import android.database.Cursor

/**
 * SQLite Database Helper Class for Storing Language Information.
 * Created by nightlock on 21/12/17.
 */
class LanguageDataManager(context: Context) : BaseCacheDataManager<Language>(context,
        DATABASE_NAME, TABLE_NAME, DATABASE_VERSION,
        COLUMN_NAME_PNC_LANGUAGE_CODE, COLUMN_MAP) {

    companion object {
        const val DATABASE_NAME = "languageDB.db"
        const val DATABASE_VERSION = 2
        const val TABLE_NAME = "languageEntry"
        const val COLUMN_NAME_INDEX = "_id"
        const val COLUMN_NAME_PNC_LANGUAGE_CODE = "pnc_lang_code"
        const val COLUMN_NAME_NAME = "name"
        const val COLUMN_NAME_LOCAL_NAME = "local_name"
        const val COLUMN_NAME_TRANSLATORS = "translators"
        const val COLUMN_NAME_ISO_LANGUAGE_CODE = "iso_lang_code"
        const val COLUMN_NAME_ISO_LANGUAGE_VERSION = "iso_lang_ver"
        val COLUMN_MAP = mapOf(
                COLUMN_NAME_INDEX to "INTEGER PRIMARY KEY AUTOINCREMENT",
                COLUMN_NAME_PNC_LANGUAGE_CODE to "TEXT",
                COLUMN_NAME_NAME to "TEXT",
                COLUMN_NAME_LOCAL_NAME to "TEXT",
                COLUMN_NAME_TRANSLATORS to "TEXT",
                COLUMN_NAME_ISO_LANGUAGE_CODE to "TEXT",
                COLUMN_NAME_ISO_LANGUAGE_VERSION to "INTEGER"
        )

        fun updateLanguage(context: Context) {
            val db = LanguageDataManager(context)
            for (language in getLanguageList()) db.put(language)
        }
    }

    fun get(index: Int): Language = get(COLUMN_NAME_INDEX, index.toString())

    override fun getFromCursor(cursor: Cursor): Language {
        val pncCode = cursor.getString(1)
        val name = cursor.getString(2)
        val localName = cursor.getString(3)
        val translators = cursor.getString(4).split(",")
        val isoCode = cursor.getString(5)
        val isoVer = cursor.getInt(6)

        return Language(name, localName, translators, pncCode, isoCode, isoVer)
    }

    override fun toContentValues(data: Language): ContentValues = ContentValues().apply {
        put(COLUMN_NAME_PNC_LANGUAGE_CODE, data.pncCode)
        put(COLUMN_NAME_NAME, data.name)
        put(COLUMN_NAME_LOCAL_NAME, data.localName)
        put(COLUMN_NAME_TRANSLATORS, data.translators.joinToString(","))
        put(COLUMN_NAME_ISO_LANGUAGE_CODE, data.isoCode)
        put(COLUMN_NAME_ISO_LANGUAGE_VERSION, data.isoVersion)
    }

    override fun getIdValue(data: Language): String = data.pncCode
}

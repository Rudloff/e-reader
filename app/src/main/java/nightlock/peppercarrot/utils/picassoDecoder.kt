/*
 * Copyright (C) 2015 -2016 Gökhan Barış Aker <gokhanbarisaker@gmail.com>, davemorrissey.
 * Converted to Kotlin by Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 */
package nightlock.peppercarrot.utils

import android.content.Context
import android.graphics.*
import android.net.Uri
import com.davemorrissey.labs.subscaleview.decoder.ImageDecoder
import com.davemorrissey.labs.subscaleview.decoder.ImageRegionDecoder
import com.jakewharton.picasso.OkHttp3Downloader
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso
import okhttp3.OkHttpClient

/**
 * SubsamplingScaleImageView Decoder class intended to be used with Picasso image loader.
 * Created by gokhanbarisaker on 8/30/15.
 */
class Decoder(private val picasso: Picasso) : ImageDecoder {
    @Throws(Exception::class)
    override fun decode(context: Context, uri: Uri): Bitmap =
            picasso
                    .load(uri)
                    .config(Bitmap.Config.RGB_565)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .get()
}

class RegionDecoder(private val client: OkHttpClient) : ImageRegionDecoder {
    private lateinit var decoder: BitmapRegionDecoder
    private val decoderLock = Any()

    @Throws(Exception::class)
    override fun init(context: Context, uri: Uri): Point {
        val downloader = OkHttp3Downloader(client)
        val inputStream = downloader.load(uri, 0).inputStream
        decoder = BitmapRegionDecoder.newInstance(inputStream, false)

        return Point(decoder.width, decoder.height)
    }

    override fun decodeRegion(rect: Rect, sampleSize: Int): Bitmap {
        synchronized(decoderLock) {
            val options = BitmapFactory.Options().apply {
                inSampleSize = sampleSize
                inPreferredConfig = Bitmap.Config.RGB_565
            }
            val bitmap = decoder.decodeRegion(rect, options)
            return bitmap
                    ?: throw RuntimeException("Region decoder returned null bitmap" +
                    " - image format may not be supported")
        }
    }

    override fun isReady(): Boolean = !decoder.isRecycled

    override fun recycle() = decoder.recycle()

}
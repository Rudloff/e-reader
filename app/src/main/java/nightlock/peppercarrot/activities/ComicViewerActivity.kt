/*
 * Copyright (C) 2017 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Reader for Pepper&Carrot.
 *
 * Reader for Pepper&Carrot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nightlock.peppercarrot.activities

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.NavUtils
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.ImageButton
import nightlock.peppercarrot.R
import nightlock.peppercarrot.adapters.ComicViewerAdapter
import nightlock.peppercarrot.utils.ArchiveDataManager
import nightlock.peppercarrot.utils.Episode
import nightlock.peppercarrot.utils.Language


class ComicViewerActivity : AppCompatActivity() {
    private val hideHandler = Handler()

    @SuppressLint("InlinedApi")
    private val hidePart2Runnable = Runnable {
        contentView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_FULLSCREEN or
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
    }
    private val mShowPart2Runnable = Runnable {
        // Delayed display of UI elements
        supportActionBar?.show()
        controlsView.visibility = View.VISIBLE
    }
    private var visibility: Boolean = false
    private val hideRunnable = Runnable { hide() }

    private val delayHideTouch = {
        delayedHide(AUTO_HIDE_DELAY_MILLIS)
        false
    }

    private val controlsView by lazy {
        findViewById<View>(R.id.fullscreen_content_controls)
    }
    private val contentView by lazy {
        findViewById<ViewPager>(R.id.comic_viewpager)
    }
    private val backButton by lazy {
        findViewById<ImageButton>(R.id.button_back)
    }
    private val forwardButton by lazy {
        findViewById<ImageButton>(R.id.button_forward)
    }


    private var episodeId = 0
    private val language by lazy {
        intent.getParcelableExtra<Language>(Language.LANGUAGE)
    }
    private val episodeDb by lazy {
        ArchiveDataManager(this)
    }

    private lateinit var comicViewerAdapter: ComicViewerAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comic_viewer)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        visibility = true

        backButton.setOnClickListener { _ ->
            contentView.setCurrentItem(contentView.currentItem - 1, true)
            delayHideTouch()
        }
        forwardButton.setOnClickListener { _ ->
            contentView.setCurrentItem(contentView.currentItem + 1, true)
            delayHideTouch()
        }
        forwardButton.setOnLongClickListener {
            if (episodeId < episodeDb.length() - 1) {
                episodeId++
                attachAdapter(episodeDb.get(episodeId), language)
            }
            delayHideTouch()
        }
        backButton.setOnLongClickListener {
            if (episodeId > 1) {
                episodeId--
                attachAdapter(episodeDb.get(episodeId), language)
            }
            delayHideTouch()
        }

        val episode = intent.getParcelableExtra<Episode>(Episode.EPISODE)
        episodeId = episode.index
        attachAdapter(episode, language)
    }

    private fun attachAdapter(episode: Episode, language: Language) {
        comicViewerAdapter = ComicViewerAdapter(episode, language, supportFragmentManager)
        comicViewerAdapter.notifyDataSetChanged()
        contentView.adapter = comicViewerAdapter
    }
    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        delayedHide(100)
    }

    override fun onResume() {
        super.onResume()
        delayedHide(100)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this)
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    fun toggle() = if (visibility) hide() else show()

    private fun hide() {
        supportActionBar?.hide()
        controlsView.visibility = View.GONE
        visibility = false

        hideHandler.removeCallbacks(mShowPart2Runnable)
        hideHandler.postDelayed(hidePart2Runnable, UI_ANIMATION_DELAY.toLong())
    }

    @SuppressLint("InlinedApi")
    private fun show() {
        contentView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        visibility = true

        hideHandler.removeCallbacks(hidePart2Runnable)
        hideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY.toLong())
    }

    private fun delayedHide(delayMillis: Int) {
        hideHandler.removeCallbacks(hideRunnable)
        hideHandler.postDelayed(hideRunnable, delayMillis.toLong())
    }

    companion object {
        private const val AUTO_HIDE_DELAY_MILLIS = 2000
        private const val UI_ANIMATION_DELAY = 250
    }
}

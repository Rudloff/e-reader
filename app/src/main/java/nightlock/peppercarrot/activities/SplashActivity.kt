/*
 * Copyright (C) 2017 - 2018 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Reader for Pepper&Carrot.
 *
 * Reader for Pepper&Carrot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nightlock.peppercarrot.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.preference.PreferenceManager
import android.view.MenuItem
import android.view.View
import com.vlonjatg.progressactivity.ProgressRelativeLayout
import io.github.mthli.sugartask.SugarTask
import kotlinx.android.synthetic.main.activity_splash.*
import nightlock.peppercarrot.R
import nightlock.peppercarrot.fragments.LanguageSelectionFragment
import nightlock.peppercarrot.utils.Language
import nightlock.peppercarrot.utils.LanguageDataManager
import java.io.IOException

class SplashActivity : AppCompatActivity() {

    @SuppressLint("ApplySharedPref")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val preference = PreferenceManager.getDefaultSharedPreferences(this)

        if (preference.getBoolean(MainActivity.SPLASH_SEEN, false)) {
            confirm_action_button.visibility = View.GONE
            loaded()
            return
        } else {
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
        }

        confirm_action_button.setOnClickListener {
            if (!preference.contains(Language.SELECTED_LANGUAGES)) {
                return@setOnClickListener
            }
            LanguageDataManager(applicationContext).close()
            preference.edit()
                    .putBoolean(MainActivity.SPLASH_SEEN, true)
                    .commit() //To make sure the value is changed *before* starting MainActivity we use a blocking commit() function.
            val mainIntent = Intent(this, MainActivity::class.java)
            startActivity(mainIntent)
            finish()
        }
        initLanguage(splash_activity)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initLanguage(progress: ProgressRelativeLayout) {
        progress.showLoading()

        SugarTask
                .with(this)
                .assign {
                    LanguageDataManager.updateLanguage(applicationContext)
                }.finish { _ ->
                    progress.showContent()
                    loaded()
                }.broken { e ->
                    if (e is IOException)
                        onError(progress, e)
                    else throw e
                }.execute()
    }

    private fun onError(progress: ProgressRelativeLayout, e: Exception) {
        progress.showError(R.drawable.ic_signal_wifi_off_black_24dp, "Network Error",
                "Connect to network and try again", "Retry", { _ ->
            initLanguage(progress)
        })
        e.printStackTrace()
    }

    private fun loaded() {
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.splash_frame, LanguageSelectionFragment())
                .commit()
    }
}
